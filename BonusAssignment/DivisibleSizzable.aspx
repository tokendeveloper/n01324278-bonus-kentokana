﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DivisibleSizzable.aspx.cs" Inherits="BonusAssignment.DivisibleSizzable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <label for="numberInput">Type a number here.</label>
                <asp:TextBox runat="server" name="numberInput" ID="numberInput"></asp:TextBox>
            </div>
            <div>
                <asp:Button runat="server" Text="Submit" OnClick="submitNum"/>
            </div>
        </div>
        <div id="determinePrime" runat="server">

        </div>

    </form>
</body>
</html>
