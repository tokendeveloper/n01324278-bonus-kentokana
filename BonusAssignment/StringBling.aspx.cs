﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class StringBling : System.Web.UI.Page
    {
        public static string ReverseString(string s)
        {
            char[] charArr = s.ToCharArray();
            Array.Reverse(charArr);
            return new string(charArr);
        }
        //Retrieved from https://stackoverflow.com/questions/228038/best-way-to-reverse-a-string
        //Takes a string as an argument.
        //Create an array of characters by declaring char[]. 
        //Use .Reverse method to reverse the order of the array items.
        //return the string by concatinating all of the charArray items.

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submitString(object sender, EventArgs e)
        {
            string userInput = stringInput.Text;
            if(ReverseString(userInput) == userInput)
            {
                output.InnerHtml = userInput + " is a palindrome";
            }
            else
            {
                output.InnerHtml = userInput + " is not a palindrome";
            }

        }

        
    }
}