﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class CartesianSmartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void getCoord(object sender, EventArgs e)
        {
            int xcoor = int.Parse(xcoord.Text);
            int ycoor = int.Parse(ycoord.Text);
            string quad = "";

            if (xcoor > 0 && ycoor > 0)
            {
                quad += "You are in quadrant I";
            }
            else if (xcoor > 0 && ycoor < 0)
            {
                quad += "You are in quadrant IV";
            }
            else if (xcoor < 0 && ycoor > 0)
            {
                quad += "You are in quadrant II";
            }
            else if (xcoor < 0 && ycoor < 0)
            {
                quad += "You are in quadrant III";
            }
            else if (xcoor == 0 || ycoor == 0) {
                quad = "Values for the coordinates cannot be 0";
            }

            quadrant.InnerHtml = quad + ".";
            
        }
    }
}