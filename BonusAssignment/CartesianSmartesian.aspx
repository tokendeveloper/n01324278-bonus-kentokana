﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CartesianSmartesian.aspx.cs" Inherits="BonusAssignment.CartesianSmartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <label for="xcoord">Type X-coordinate here.</label>
                <asp:TextBox runat="server" name="xcoord" ID="xcoord"></asp:TextBox>
            </div>

            <div>
                <label for="ycoord">Type Y-coordinate here.</label>
                <asp:TextBox runat="server" name="ycoord" ID="ycoord"></asp:TextBox>
            </div>
            <div>
                <asp:Button runat="server" Text="Submit" OnClick="getCoord"/>
            </div>

            <div id="quadrant" runat="server">

            </div>
        </div>
    </form>
</body>
</html>
