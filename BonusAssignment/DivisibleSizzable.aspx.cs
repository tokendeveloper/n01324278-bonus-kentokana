﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class DivisibleSizzable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submitNum(object sender, EventArgs e)
        {
            int num = int.Parse(numberInput.Text);
            bool isPrime = true;
            string test = "";

            if (num == 2)
            {
                isPrime = true;
            }
            else if (num == 1)
            {
                isPrime = false;
            }
            else
            {
                for(int i = num-1; i > 1; i--)
                {   
                    if(num%i == 0)
                    {
                        test += i + "<br />";
                        isPrime = false;
                    } 
                }

            }

            if (isPrime == true)
            {
                determinePrime.InnerHtml = test + num + " is a prime number";
            }
            else
            {
                determinePrime.InnerHtml = num + " is not a prime number.";
            }
        }
    }
}

//isPrime if number is dvisible by itself, and by 1 only. 
